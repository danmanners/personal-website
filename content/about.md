---
title: "About Me"
date: 2018-12-02T11:57:45-05:00
draft: false
---
# Who the heck is Dan Manners?

Dan Manners grew up in Westchester, NY. Between 2010 and 2014, he worked for [Production Resource Group](https://www.prg.com/) as a Systems Administrator I/II locally in their New Jersey, New York, and Los Angeles Offices. While there, he focused on learning and understanding how users need to operate and how to optimize workflows and backend resources to better assist his end users. In addition to this, he learned a great deal about Cisco VoIP, Layer 3 Networking, Desktop and Server deployment, and how to translate "**Beep-Boop**" to human-speak. While this may sound silly, this allowed Dan to clearly communicate with many of the higher-level executives and managers.

In mid 2014, he left PRG for [TOURtech](https://www.tourtech.com/). TOURtech offered a new array of challenges, and he went from supporting several smaller offices and (roughly) 3,000 global users to temporarily supporting networks for events like music festivals, concert tours, corporate tradeshows, and product launches that could handle upwards of _**30,000**_ wired and wireless clients. The networks would be deployed for services like Point-of-Sales systems, Access Control, CCTV, Guest Services, Sponsorship booths, and public and private wifi.

At the beginning of 2017, Dan began working with [BrainGu](https://braingu.com). While it may be considered a radical shift from his experience with PRG and TOURtech, this was a brand new style of challenge. Most of what he has worked on is either under strict NDA or classified, but what he **CAN** talk about is [Structsure](https://structsure.com). During his work on Structsure, he primarily focused on learning what good and bad DevOps workflows look like.

Then in March of 2019, he re-joined TOURtech as the Senior Network Engineer, specializing in systems and design. He worked on designing and rolling out comprehensive monitoring tools, automated tooling to allow rapid configuration and deployment, and made sure that every TOURtech network ties its management back to the home office for remote support and management. During his time with TOURtech the second time around, his big claim-to-fame was designing the network architecture for a cellular carriers 4G to WiFi offload for roughly 11,000 concurrent devices at a major sporting event. Unfortunately a year to the day after joining again mid-COVID-19 crisis, he was laid off.

Only a few days later, he was fortunate enough to be introduced to [TEKsystems](https://www.teksystems.com/) and was hired as a DevOps Engineer for [Cisco](https://www.cisco.com/)! At Cisco, he's working on building automation tooling and pipelines to securely deploy and simplify code and package sharing between several internal services and partner locations in zero-trust environments.

Throughout his career spanning over a decade, Dan has had one constant motto: "Be good, do others good, and don't be a dick." While a bit forward, it translates well to all facets of life.

## What's Dan been working on? 

Recently, Dan's been working on automating infrastructure deployment with Puppet Bolt. In environments where Puppet is already in full swing, building plans with Puppet Bolt over Ansible allows re-use of existing code and logic. Additionally, he's been deploying and utilizing Jenkins, SonarQube, Artifactory, and the Atlassian toolset in zero-trust environments while load balancing it all with HAProxy.

In his personal time, he's been building out tooling to deploy baremetal Kubernetes with minimal effort.

## Things to know about Dan:

```yaml
Personal_Information:
	Hometown: "Westchester, NY"
	Pets:
		Dog:
			Name:  "Marcel"
			Born:   "~2014"
			Breed: "Mutt; Lab/Shepard/Rottweiler"
	Food_Preference:
		Primary:   "Japanese Cuisine"
		Secondary: "French Cuisine"
		Tertiary:  "American Cuisine"
	Siblings: 2
	Favorite_Vacation: "Tokyo, Japan"
---
Homelab_Hardware:
	ISP:
		Spectrum:
			Status: "Active"
			Speed:
				Download: "200 Mbps"
				Upload:   "10 Mbps"
		Ting:
			Status: "Pending"
			Speed:
				Download: "1000 Mbps"
				Upload:   "1000 Mbps"
	Routing:
		Mikrotik:
			Model: "CCR1009-7G-1C-1S+"
			Ports: 
				GigE: 8
				SFP+: 1
				SFP:  1
	Switching:
		Core_Switch:
			Model: "Netonix WS-12-250AC"
			Ports:
				GigE: 12
				SFP:  2
		Upstairs_Expansion:
			Model: "Ubiquiti EdgeSwitch 8"
			Ports:
				GigE: 8
				SFP:  2
	Storage:
		Synology:
			Model:    "DS1815+"
			Drives:   8
			Storage:  "14TB"
			Ports:
				GigE: 4
	Wireless:
		Upstairs:
			Model: "Unifi AP AC Pro"
		Kitchen:
			Model: "Unifi AP AC In-Wall"
		MasterBedroom:
			Model: "Unifi AP AC In-Wall"
	CCTV:
		NVR_Software: 
			- "DigitalWatchdog IPVMS"
			- "Synology Surveillance Station"
		Camera_Count: 5
```

## Okay, that's all great.
What technologies, hardware, and software does he have experience with though?


### DevOps Automation & CI/CD

<center>
	<a href="https://www.ansible.com/">
		<img src="images/ansible.webp" width="20%" alt="Ansible">
	</a>
	<a href="https://puppet.com/docs/bolt/latest/bolt.html">
		<img src="images/puppetbolt.png" width="20%" alt="Puppet Bolt">
	</a>
	<a href="https://jenkins.io/">
		<img src="images/jenkins.png" width="15%" alt="Jenkins">
	</a>
	<a href="https://www.terraform.io/">
		<img src="images/terraform.png" width="20%" alt="Terraform">
	</a>
</center>

### Containerization and Orchestration

<center>
	<a href="https://www.docker.com/">
		<img src="images/docker.png" width="30%" alt="Docker">
	</a>
	<a href="https://kubernetes.io/">
		<img src="images/kubernetes.png" width="30%" alt="Kubernetes">
	</a>
	<a href="https://www.docker.com/">
		<img src="images/dockerswarm.png" width="30%" alt="Docker Swarm">
	</a>
</center>

### Cloud Providers

<center>
	<a href="https://www.digitalocean.com/">
		<img src="images/digitalocean.png" width="30%" alt="Digital Ocean"
			style="margin: 0px 30px">
	</a>
	<a href="https://aws.amazon.com/">
		<img src="images/aws.png" width="30%" alt="AWS"
			style="margin: 20px 30px">
	</a>
</center>

### Applications & Services

<center>
	<a href="https://www.librenms.org/">
		<img src="images/librenms.png" width="18%" alt="LibreNMS">
	</a>
	<a href="https://www.freeipa.org/page/Main_Page">
		<img src="images/freeipa.png" width="18%" alt="FreeIPA">
	</a>
	<a href="https://www.vaultproject.io/">
		<img src="images/vault.png" width="18%" alt="Vault">
	</a>
	<a href="https://gitlab.com/">
		<img src="images/gitlab.png" width="18%" alt="Gitlab">
	</a>
	<a href="https://rocket.chat/">
		<img src="images/rocketchat.png" width="18%" alt="RocketChat">
	</a>
	<a href="https://www.atlassian.com/">
		<img src="images/atlassian.png" width="18%" alt="Atlassian Toolset"
			style="margin: 20px 0px 0px 0px">
	</a>
</center>

### Operating Systems

<center>
	<a href="https://www.centos.org/">
		<img src="images/centos.png" width="20%" alt="CentOS 7">
	</a>
	<a href="https://www.ubuntu.com/">
		<img src="images/ubuntu-logo.png" width="40%" alt="Ubuntu 14.04+"
			style="margin: 0px 20px">
	</a>
	<a href="https://aws.amazon.com/amazon-linux-ami/">
		<img src="images/amazonlinux.png" width="20%" alt="Amazon Linux">
	</a>
	<br><br>
	<a href="https://www.microsoft.com/en-us/evalcenter/evaluate-windows-server-2012-r2/">
		<img src="images/winserver2012.png" width="45%" 
			alt="Windows Server 2012" style="margin: 20px 0px">
	</a>
	<br><br>
	<a href="https://www.pfsense.org/">
		<img src="images/pfsense.png" width="35%" alt="pfSense">
	</a>
</center>

### Hardware

<center>
	<a href="https://www.cisco.com/">
		<img src="images/cisco.png" width="20%" alt="Cisco"
			style="margin: 20px 10px 10px 0px">
	</a>
	<a href="https://www.juniper.net">
		<img src="images/juniper.png" width="25%" alt="Juniper"
			style="margin: 5px 0px" style="padding-right: 40px;">
	</a>
	<a href="https://www.edge-core.com/">
		<img src="images/edge-core.png" width="30%" alt="Edge-Core"
			style="margin: 22px 20px">
	</a>
	<br>
	<a href="https://meraki.cisco.com/">
		<img src="images/meraki.png" width="20%" alt="Meraki"
			style="margin: 0px 30px">
	</a>
	<a href="https://www.ubnt.com/">
		<img src="images/ubnt.png" width="15%" alt="Ubiquiti">
	</a>
	<a href="https://mikrotik.com//">
		<img src="images/mikrotik.svg" width="30%" alt="Meraki"
			style="margin: 25px 20px">
	</a>
	<br>
	<a href="https://www.synology.com/">
		<img src="images/synology.png" width="35%" alt="Synology"
			style="margin: 20px 0px">
	</a>
	<a href="https://www.ruckuswireless.com/">
		<img src="images/ruckus.png" width="15%" alt="Ruckus Wireless"
			style="margin: 10px 0px">
	</a>
</center>

### Programming/Scripting

<center>
	<a href="https://en.wikipedia.org/wiki/Bash_(Unix_shell)">
		<img src="images/bash.png" width="20%" alt="Bash" 
			style="margin: 0px 20px 0px 0px">
	</a>
	<a href="https://www.python.org/">
		<img src="images/python.png" width="22%" alt="Python3"
			style="margin: 0px 25px">
	</a>
	<a href="https://golang.org/">
		<img src="images/golang.png" width="25%" alt="Go">
	</a>
</center>
